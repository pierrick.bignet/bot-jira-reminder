const gulp = require('gulp');
const babel = require('gulp-babel');

const jiraClientPath = 'lib/jira-client/src/*.js';

gulp.task('default', () =>
  gulp.src(jiraClientPath)
    .pipe(babel({
      presets: ['es2015']
    }))
    .pipe(gulp.dest('lib/jira-client/lib'))
);

gulp.task('watch', function(){
  gulp.watch(jiraClientPath, ['default']);
});